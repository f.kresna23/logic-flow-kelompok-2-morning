// Declare variable berkas
const berkas = ["KTP", "Form Pendaftaran"];
// Declare syarat kesehatan
const kesehatan = ["Tensi Darah", "Suhu"];
// Declare ketersediaan kartu vaksin
const kartuVaksin = ["Kartu Vaksin Tercetak"];

// Fungsi registrasi vaksin
function regisVaksin() {
  console.log("Daftar di peduli lindungi");
}

// Fungsi cek berkas
function cekBerkas() {
  if (berkas.includes("KTP", "Form Pendaftaran")) {
    console.log("Berkas lengkap, lanjut tes kesehatan");
    return true;
  }
  console.log("Berkas tidak lengkap, mohon lengkapi terlebih dahulu");
  return false;
}

// Fungsi melengkapi berkas
function lengkapiBerkas() {
  console.log("Lengkapi KTP");
  console.log("Isi Form Pendaftaran");
}

// Fungsi tes kesehatan
function tesKesehatan() {
  console.log("Tes Kesehatan Terlebih Dahulu sebelum Vaksin");
}
// Fungsi cek kesehatan
function cekKesehatan() {
  if (kesehatan.includes("Tensi Darah", "Suhu")) {
    console.log("Tensi dan Suhu tubuh aman. Lanjut Vaksin !");
    return true;
  }
  console.log("Tensi atau suhu tidak aman. Silahkan kembali seminggu lagi !");
  return false;
}

// Fungsi tervaksinasi
function Vaksin() {
  console.log("Anda telah divaksin. Silahkan ambil kartu vaksin ke petugas !");
}

// Fungsi cetak kartu vaksin
function cetakKartu() {
  if (kartuVaksin.includes("Kartu Vaksin Tercetak")) {
    console.log(
      "Proses Vaksinasi Selesai. Silahkan datang 1 bulan lagi untuk vaksin kedua !"
    );
    return true;
  }
  console.log(
    "Kartu vaksin belum tercetak, silahkan kembali 1 minggu lagi untuk cetak kartu !"
  );
  return false;
}

// Print keseluruhan fungsi
function start() {
  regisVaksin();
  !cekBerkas() && lengkapiBerkas;
  tesKesehatan();
  !cekKesehatan();
  Vaksin();
  !cetakKartu();
}

start();
